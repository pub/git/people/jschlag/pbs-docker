#!/bin/bash
#
#settings
#
#="database"

log() {
	echo "#######################################"
	echo ""
	echo "$@"
	echo ""
	echo "#######################################"
}

# build the pbs from master branch
log "Update all packages"
yum update -y
log "Installing epel"
yum install -y epel-release
build_deps="git gcc make autoconf libtool intltool libcap-devel libsolv-devel python-devel xz-devel"
deps="libcap libsolv xz python"

log "Installing dependencies"
yum install -y ${deps}
log "Installing packages for build"
yum install -y ${build_deps}

mkdir -p /opt/dev
log "Building pakfire"
cd /opt/dev
git clone -b master  http://git.ipfire.org/pub/git/pakfire.git
cd pakfire
./autogen.sh
./configure --prefix=/usr
make
make install

# cleanup
yum remove -y ${build_deps}
yum autoremove -y
yum clean all
rm -rf /var/tmp/* /var/cache/yum/* /opt/dev/*
