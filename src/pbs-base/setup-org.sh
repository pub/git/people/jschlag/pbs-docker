#!/bin/bash
#
#settings
#
#="database"

log() {
	echo "#######################################"
	echo ""
	echo "$@"
	echo ""
	echo "#######################################"
}

# build the pbs from master branch
log "Update all packages"
yum update -y
log "Installing epel"
yum install -y epel-release
deps="python python-tornado python-daemon"

log "Installing dependencies"
yum install -y ${deps}


# add a user
groupadd -g 2000 pbs
useradd --system --no-create-home --shell /bin/bash --uid 2000 --gid 2000 pbs

mkdir -p /opt/pbs

# set permissions
chown -R pbs:pbs /opt/pbs

# cleanup
yum autoremove -y
yum clean all
rm -rf /var/tmp/* /var/cache/yum/* /opt/dev/*
